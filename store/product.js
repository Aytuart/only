import { $urls } from '~/common/urls.js'

export const state = () => ({
    newproducts: null,
    hitproducts: null,
    saleproducts: null,
    favorites: null,
    products: null,
    recomendedproducts: null,
    product: null,
    brand: null,
    reviews: {},
    questions: {},
    searchResults: null,
})

export const getters = {
    GET_PRODUCT: (state) => state.product,
    GET_BRAND: (state) => state.brand,
    GET_PRODUCTS: (state) => state.products,
    GET_FAVORITES: (state) => state.favorites,
    GET_NEWS: (state) => state.newproducts,
    GET_HITS: (state) => state.hitproducts,
    GET_SALES: (state) => state.saleproducts,
    GET_SEARCH_RESULTS: (state) => state.searchResults,
    GET_REVIEWS: (state) => state.reviews,
    GET_QUESTIONS: (state) => state.questions,
    GET_RECOMENDED: (state) => state.recomendedproducts
}


export const mutations = {
    SET_PRODUCT: (state, payload) => state.product = payload,
    SET_BRAND: (state, payload) => state.brand = payload,
    SET_PRODUCTS: (state, payload) => state.products = payload,
    SET_RECOMENDED: (state, payload) => state.recomendedproducts = payload,
    SET_NEWS: (state, payload) => state.newproducts = payload,
    SET_FAVORITES: (state, payload) => state.favorites = payload,
    SET_HITS: (state, payload) => state.hitproducts = payload,
    SET_SALES: (state, payload) => state.saleproducts = payload,
    SET_REVIEWS: (state, payload) => state.reviews = payload,
    SET_QUESTIONS: (state, payload) => state.questions = payload,
    SET_SEARCH_RESULTS: (state, payload) => state.searchResults = payload,
    PAGINATE: (state, payload) => {
        state[payload.key].current_page = payload.value.current_page
        state[payload.key].last_page = payload.value.last_page
        state[payload.key].to = payload.value.to
        for(let i=0;i<payload.value.data.length;++i) {
            state[payload.key].data.push(payload.value.data[i])
        }
    }
}

export const actions = {
    async fetchProduct({ commit }, route) {
        if (route.params.product) {
            commit('SET_PRODUCT', await this.$api.get($urls.url('product', [route.params.category, route.params.subcategory, route.params.product]), {}, null, 'product'))
        } else {
            commit('SET_PRODUCT', null)
        }
    },
    async fetchRecomended({commit}, route) {
        if (route.params.product) {
            commit('SET_RECOMENDED', await this.$api.get($urls.url('productRecomend', [route.params.category, route.params.subcategory, route.params.product]), {}, 'recomendedproducts', 'recomended'))
        }
    },
    async fetchBrand({ commit }, route) {
        if (!route.params.category && route.query.brand) {
            commit('SET_BRAND', await this.$api.get($urls.url('brand', [route.query.brand]), {}, null, 'brand'))
        } else {
            commit('SET_BRAND', null)
        }
    },
    async fetchReviews({ commit }, route) {
        if (route.params.product) {
            commit('SET_REVIEWS', await this.$api.get($urls.url('product', [route.params.product, 'review']), {}, 'reviews', 'reviews'))
        } else {
            commit('SET_REVIEWS', {})
        }
    },
    async fetchQuestions({ commit }, route) {
        if (route.params.product) {
            commit('SET_QUESTIONS', await this.$api.get($urls.url('product', [route.params.product, 'review']), {'question': 1}, 'questions', 'questions'))
        } else {
            commit('SET_QUESTIONS', {})
        }
    },
    async paginate({ commit, state }, payload) {
        if (state[payload].current_page != state[payload].last_page) {
            commit('PAGINATE', {
                key: payload,
                value: await this.$api.paginate(payload, state[payload].current_page + 1)
            })
        }
    },
    async fetchProducts({ commit }, route) {
        let filters = {
            filters: {...route.query},
            random: new Date().getUTCMilliseconds()
        }
        if (route.query.page) {
            delete filters.filters.page
            filters.page = route.query.page
        }
        commit('SET_PRODUCTS', await this.$api.get($urls.url('catalog', [route.params.category, route.params.subcategory]), filters, 'products'))
    },
    async search({ commit }, q) {
        if (q && q.length > 1) {
            commit('SET_SEARCH_RESULTS', await this.$api.get('catalog', { filters: { q: q } }))
        } else {
            commit('SET_SEARCH_RESULTS', null)
        }
    },
    async fetchNews({ commit }, route) {
        let filters = {
            filters: {...route.query},
            random: new Date().getUTCMilliseconds()
        }
        if (route.query.page) {
            delete filters.filters.page
            filters.page = route.query.page
        }

        filters.filters['extra'] = ["news"]
        commit('SET_NEWS', await this.$api.get('catalog', filters, 'newproducts'))
    },
    async fetchHits({ commit }, route) {
        let filters = {
            filters: {...route.query},
            random: new Date().getUTCMilliseconds()
        }
        if (route.query.page) {
            delete filters.filters.page
            filters.page = route.query.page
        }

        filters.filters['extra'] = ["hits"]
        commit('SET_HITS', await this.$api.get('catalog', filters, 'hitproducts'))
    },
    async fetchSales({ commit }, route) {
        let filters = {
            filters: {...route.query},
            random: new Date().getUTCMilliseconds()
        }
        if (route.query.page) {
            delete filters.filters.page
            filters.page = route.query.page
        }

        filters.filters['extra'] = ["sales"]
        commit('SET_SALES', await this.$api.get('catalog', filters, 'saleproducts'))
    },
    async fetchFavorites({ commit, dispatch }, ids) {
        commit('SET_FAVORITES', await dispatch('getProductsByIds', ids))
    },
    async getProductsByIds({}, ids) {
        return await this.$api.get('catalog', { filters: { ids: ids }, all: true }, 'favorites')
    }
}
