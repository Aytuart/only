import { $urls } from '~/common/urls.js'

export const state = () => ({
    filters: null
})

export const getters = {
    GET_FILTERS: (state) => state.filters
}

export const mutations = {
    SET_FILTERS: (state, filters) => state.filters = filters
}

export const actions = {
    async fetchFilters({ commit }, route) {
        let filters = {...route.query}
        commit('SET_FILTERS', await this.$api.get($urls.url('filter', [route.params.category, route.params.subcategory]), { filters: filters }))
    }
}
