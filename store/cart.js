import Cookies from 'js-cookie'

export const state = () => ({
    items: {}
})

export const getters = {
    GET_ITEMS: (state) => state.items,
    GET_CNT: (state) => Object.keys(state.items).length,
    GET_CART_CNT: (state) => (product_id) => state.items[product_id] ? state.items[product_id].cnt : 0
}

export const mutations = {
    FILTER_ITEMS: (state) => {
        let items = {}, cookies = {}
        Object.keys(state.items).forEach((id) => {
            if (state.items[id].cnt) {
                items[id] = {
                    cnt: Math.abs(state.items[id].cnt),
                    product: state.items[id].product
                }
                cookies[id] = {
                    cnt: Math.abs(state.items[id].cnt),
                }
            }
        })
        state.items = Object.assign({}, items)
        Cookies.set('cart', cookies)
    },
    SET_ITEMS: (state, payload) => {
        state.items = payload
    },
    SET_ITEM: (state, payload) => {
        state.items = Object.assign({}, state.items, {...payload})
    }
}

export const actions = {
    async fetchProducts({ commit, dispatch }, items) {
        items = JSON.parse(items ? items : '{}')
        let products = await dispatch('product/getProductsByIds', Object.keys(items), { root: true })

        let new_items = {}

        products.data.forEach(product => {
            new_items[product.id] = {}
            new_items[product.id].product = product
            new_items[product.id].cnt = items[product.id].cnt// TODO: set min and noitify Math.min(items[product.id].cnt, product.quantity)
        })

        commit('SET_ITEMS', new_items)
        commit('FILTER_ITEMS')
    },
    removeProduct({ commit }, product) {
        commit('SET_ITEM', {
            [product.id]: {
                cnt: 0
            }
        })
        commit('FILTER_ITEMS')
    },
    increaseProduct({ commit, state }, product) {
        if (!state.items[product.id]) {
            this.$toastr.success('Товар добавлен в корзину')
        }
        commit('SET_ITEM', {
            [product.id]: {
                cnt: state.items[product.id] ? state.items[product.id].cnt + 1 : 1,
                product: product,
            }
        })
        commit('FILTER_ITEMS')
    },
    decreaseProduct({ commit, state }, product) {
        commit('SET_ITEM', {
            [product.id]: {
                cnt: state.items[product.id] ? state.items[product.id].cnt - 1 : 0,
                product: product,
            }
        })
        commit('FILTER_ITEMS')
    },
    setProductCnt({ commit }, payload) {
        commit('SET_ITEM', {
            [payload.product.id]: {
                cnt: payload.cnt,
                product: payload.product,
            }
        })
        commit('FILTER_ITEMS')
    }
}
