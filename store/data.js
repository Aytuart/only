export const state = () => ({
    data: {}
})

export const getters = {
    GET: (state) => (key) => state.data[key]
}

export const mutations = {
    SET: (state, value) => {
        state.data = Object.assign({}, state.data, value)
    }
}
