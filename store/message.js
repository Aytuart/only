export const state = () => ({
    messages: {},
    cnt: 0,
})

export const getters = {
    GET_MESSAGES: (state) => state.messages
}

export const mutations = {
    ADD_MESSAGE: (state, payload) => {0
        state.messages[state.cnt++] = payload
        state.messages = Object.assign({}, state.messages)
    },
    DELETE_MESSAGE: (state, index) => {
        delete state.messages[index]
        state.messages = Object.assign({}, state.messages)
    }
}

export const actions = {
    show({ commit, state }, payload) {
        commit('ADD_MESSAGE', payload)
        return state.cnt - 1
    },
    close({ commit }, payload) {
        commit('DELETE_MESSAGE', payload)
    }
}
