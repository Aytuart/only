export const state = () => ({
    delivery_costs: {}
})

export const getters = {
    GET_DELIVERY_COSTS: (state) => state.delivery_costs
}

export const mutations = {
    SET_DELIVERY_COSTS: (state, deliveryCosts) => {
        state.delivery_costs = deliveryCosts
    }
}

export const actions = {
    async fetchDeliveryCosts({ commit }, payload) {
        commit('SET_DELIVERY_COSTS', await this.$api.get('deliveryCost', payload))
    },
    setDeliveryCosts({ commit }, payload) {
        commit('SET_DELIVERY_COSTS', payload)
    }
}
