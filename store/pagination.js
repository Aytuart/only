export const state = () => ({
    paginations: {}
})

export const getters = {
    GET_PAGINATION: (state) => (key) => state.paginations[key]
}

export const mutations = {
    SET_PAGINATION: (state, payload) => state.paginations[payload.key] = payload.value
}
