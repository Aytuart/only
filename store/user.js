import { $urls } from '~/common/urls.js'

export const state = () => ({
    user: null,
    addresses: null,
    orders: null,
    bonuses: null,
})

export const getters = {
    GET_USER: (state) => state.user,
    GET_ADDRESSES: (state) => state.addresses,
    GET_ORDERS: (state) => state.orders,
    GET_BONUSES: (state) => state.bonuses
}

export const mutations = {
    SET_USER: (state, payload) => {
        state.user = payload
    },
    SET_USER_PHONE: (state, payload) => {
        state.user.phone = payload
    },
    SET_ADDRESSES: (state, payload) => {
        state.addresses = payload
    },
    SET_ORDERS: (state, payload) => {
        state.orders = payload
    },
    SET_BONUSES: (state, payload) => {
        state.bonuses = payload
    },
    UPDATE_USER: (state, payload) => {
        state.user = Object.assign({}, { ...state.user }, payload)
    }
}

export const actions = {
    async fetchUser({ commit, dispatch }, payload) {
        let resp = await this.$api.get('user.profile', {}, 'fetchUser')

        if (this.$api.getError('fetchUser')) {
            dispatch('auth/logout', null, { root: true })
        } else {
            commit('SET_USER', resp)
            commit('favorites/SET_FAVORITES', resp.favorites, { root: true })
        }
    },

    async fetchAddresses({ commit }, payload) {
        commit('SET_ADDRESSES', await this.$api.get('user.address'))
    },

    async fetchOrders({ commit }, payload) {
        commit('SET_ORDERS', await this.$api.get('user.order'))
    },

    async cancelOrder({ commit, dispatch }, payload) {
        await this.$api.delete($urls.url('user.order', [payload]))
        await dispatch('fetchOrders')
    },

    async fetchBonuses({ commit }, payload) {
        commit('SET_BONUSES', await this.$api.get('user.bonus'))
    },

    async updateLogin({ commit }, payload) {
        await this.$api.put('auth.user', { login: payload.login }, payload.error)
    },

    async verifyLogin({ commit }, payload) {
        let error = payload.error ? payload.error : 'profile.phone.verify'
        delete payload.error

        await this.$api.get('auth.verify', payload, null, error)
        if (!this.$api.getError('profile.phone.verify') && this.$isSignedIn()) {
            commit('SET_USER_PHONE', payload.login)
        }
    },

    async updatePassword({}, payload) {
        await this.$api.put('auth.password', payload, 'profile.password.old')
    },

    async updateUser({ commit }, payload) {
        commit('UPDATE_USER', payload)
        await this.$api.put('user', payload, Object.keys(payload).join('_'))
    },
}
