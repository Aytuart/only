const cookieparser = process.server ? require("cookieparser") : undefined;
import Cookies from "js-cookie";

export const state = () => ({
    isMobile: false,
    errors: {},
    isAccepted: false
});

export const getters = {
    GET_IS_MOBILE: state => state.isMobile,
    GET_ERROR: state => key => state.errors[key],
    IS_COOKIES_ACCPETED: state => state.isAccepted
};

export const mutations = {
    SET_IS_MOBILE: (state, payload) => (state.isMobile = payload),
    SET_ERROR: (state, payload) =>
        (state.errors = Object.assign({}, state.errors, { ...payload })),
    SET_ERRORS: (state, payload) => (state.errors = payload),
    ACCEPT_COOKIES: (state, p) => (state.isAccepted = true)
};

export const actions = {
    async nuxtServerInit({ dispatch, commit }, { req, res, app }) {
        commit("SET_IS_MOBILE", req.headers["x_is_mobile"] == "true");
        // commit("SET_IS_MOBILE", 1);
        let cookies = cookieparser.parse(String(req.headers.cookie));
        if (cookies.accepted) {
            commit("ACCEPT_COOKIES", true, { expires: 365 * 4 });
        }

        let queue = [
            dispatch("settings/fetchSettings"),
            dispatch("content/fetchZoodpayConfiguration"),
            dispatch("category/fetchCategories"),
            dispatch("cart/fetchProducts", cookies.cart),
            dispatch("auth/signinFromCookies", cookies.AuthToken),
            dispatch("favorites/fetchProducts", cookies.favorites),
            dispatch("instagram/fetch")
        ];

        if (!app.$isSignedIn()) {
            res.setHeader("Set-Cookie", `AuthToken=`);
        }
        return Promise.all(queue);
    },
    acceptCookies({ commit }) {
        commit("ACCEPT_COOKIES");
        Cookies.set("accepted", 1, { expires: 2 * 365 });
    }
};
