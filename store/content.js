import { $urls } from '~/common/urls.js'

export const state = () => ({
    banners: null,
    posts: null,
    lastPosts: null,
    post: null,
    shops: null,
    cities: null,
    paymentTypes: null,
    deliveryTypes: null,
    zoodpayConfiguration: null
})

export const getters = {
    GET_BANNERS: (state) => state.banners,
    GET_POSTS: (state) => state.posts,
    GET_LAST_POSTS: (state) => state.lastPosts,
    GET_POST: (state) => state.post,
    GET_SHOPS: (state) => state.shops,
    GET_CITIES: (state) => state.cities,
    GET_PAYMENT_TYPES: (state) => state.paymentTypes,
    GET_DELIVERY_TYPES: (state) => state.deliveryTypes,
    GET_ZOODPAY_CONFIGURATION: (state) => state.zoodpayConfiguration
}

export const mutations = {
    SET_BANNERS: (state, banners) => state.banners = banners,
    SET_POSTS: (state, posts) => state.posts = posts,
    SET_LAST_POSTS: (state, posts) => state.lastPosts = posts,
    SET_POST: (state, post) => state.post = post,
    SET_SHOPS: (state, shops) => state.shops = shops,
    SET_CITIES: (state, cities) => state.cities = cities,
    SET_PAYMENT_TYPES: (state, payload) => state.paymentTypes = payload,
    SET_DELIVERY_TYPES: (state, payload) => state.deliveryTypes = payload,
    SET_ZOODPAY_CONFIGURATION: (state, payload) => state.zoodpayConfiguration = payload,
    PAGINATE: (state, payload) => {
        state[payload.key].current_page = payload.value.current_page
        state[payload.key].last_page = payload.value.last_page
        state[payload.key].to = payload.value.to
        for(let i=0;i<payload.value.data.length;++i) {
            state[payload.key].data.push(payload.value.data[i])
        }
    }
}

export const actions = {
    async fetchBanners({ commit }, payload) {
        commit('SET_BANNERS', await this.$api.get('banner'))
    },
    async fetchPosts({ commit }, route) {
        commit('SET_POSTS', await this.$api.get('post', route.query, 'posts'))
    },
    async fetchLastPosts({ commit }, route) {
        commit('SET_LAST_POSTS', await this.$api.get('post', {}, 'posts'))
    },
    async fetchPost({ commit }, route) {
        commit('SET_POST', await this.$api.get($urls.url('post', [route.params.post]), {}, null, 'post'))
    },
    async fetchShops({ commit }, payload) {
        commit('SET_SHOPS', await this.$api.get('shop'))
    },
    async fetchCities({ commit }, payload) {
        commit('SET_CITIES', await this.$api.get('city'))
    },
    async fetchPaymentTypes({ commit }, payload) {
        commit('SET_PAYMENT_TYPES', await this.$api.get('payment'))
    },
    async fetchDeliveryTypes({ commit }, payload) {
        commit('SET_DELIVERY_TYPES', await this.$api.get('delivery'))
    },
    async fetchZoodpayConfiguration({ commit }, payload) {
        try {
            commit('SET_ZOODPAY_CONFIGURATION', await this.$api.get('zoodpay.configuration'))
        } catch (e) {
            //
        }
    },
    async paginate({ commit, state }, payload) {
        if (state[payload].current_page != state[payload].last_page) {
            commit('PAGINATE', {
                key: payload,
                value: await this.$api.paginate(payload, state[payload].current_page + 1)
            })
        }
    },

}
