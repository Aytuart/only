import { $urls } from '~/common/urls.js'

export const state = () => ({
    settings: {}
})

export const getters = {
    GET_SETTINGS: (s) => s.settings,
    GET_BY_KEY: (s) => (key) => s.settings[key] ? s.settings[key].value : s.settings[key]
}

export const mutations = {
    SET_SETTINGS: (s, p) => s.settings = p
}

export const actions = {
    async fetchSettings({ commit }, p) {
        commit('SET_SETTINGS', await this.$api.get($urls.url('settings')))
    }
}
