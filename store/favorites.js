import Cookies from 'js-cookie'
import { $urls } from '~/common/urls.js'

export const state = () => ({
    favorites: [],
})

export const getters = {
    GET_FAVORITES: (state) => state.favorites,
    IS_FAVORITE: (state) => (product) => state.favorites.indexOf(typeof product == 'object' ? product.id : product) != -1
}

export const mutations = {
    SET_FAVORITES: (state, payload) => {
        state.favorites = payload
    },
}

export const actions = {
    async fetchProducts({ commit }, favorite_ids) {
        if (!this.$isSignedIn()) {
            favorite_ids = JSON.parse(favorite_ids ? favorite_ids : '[]')
            if (Array.isArray(favorite_ids)) {
                commit('SET_FAVORITES', favorite_ids)
            }
        }
    },
    async toggleProduct({ commit, state }, product) {
        let favorites = [...state.favorites]
        let index = favorites.indexOf(product.id)

        if (this.$isSignedIn()) {
            await this.$api.post($urls.url('user.product', [product.slug]))
            if (this.$api.getError('default')) {
                // TODO: notify
            }
        }

        if (index != -1) {
            favorites.splice(index, 1)
        } else {
            favorites.push(product.id)
        }

        commit('SET_FAVORITES', favorites)
        if (!this.$isSignedIn()) {
            Cookies.set('favorites', state.favorites)
        }
    }
}
