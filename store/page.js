import { $urls } from '~/common/urls.js'

export const state = () => ({
    page: null,
})

export const getters = {
    GET_PAGE: (state) => state.page
}

export const mutations = {
    SET_PAGE: (state, page) => state.page = page
}

export const actions = {
    async fetchPage({ commit }, slug) {
        commit('SET_PAGE', await this.$api.get($urls.url('page', [slug ? slug : 'home'])))
    }
}
