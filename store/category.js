export const state = () => ({
    categories: null,
    category: null,
    subcategory: null
})

export const getters = {
    GET_CATEGORIES: (state) => Object.values(state.categories),
    GET_CATEGORY: (state) => state.category,
    GET_SUBCATEGORY: (state) => state.subcategory
}

export const mutations = {
    SET_CATEGORIES: (state, payload) => state.categories = payload,
    SET_CATEGORY: (state, payload) => state.category = payload,
    SET_SUBCATEGORY: (state, payload) => state.subcategory = payload
}

export const actions = {
    async fetchCategories({ commit }, payload) {
        commit("SET_CATEGORIES", await this.$api.get('category'))
    },
    setCategory({ commit, state }, route) {
        if (!route.params.category) {
            commit('SET_CATEGORY', null)
            return true
        }

        if (state.categories[route.params.category]) {
            commit('SET_CATEGORY', state.categories[route.params.category])
            return true
        }

        commit('SET_CATEGORY', null)
        return false
    },
    setSubCategory({ commit, state }, route) {
        if (!route.params.subcategory) {
            commit('SET_SUBCATEGORY', null)
            return true
        }

        if (state.category.subcategories[route.params.subcategory]) {
            commit('SET_SUBCATEGORY', state.category.subcategories[route.params.subcategory])
            return true
        }
        commit('SET_SUBCATEGORY', null)
        return false
    }
}
