export const state = () => ({
    brands: null,
    search_brands: null
})

export const getters = {
    GET_BRANDS: state => state.brands,
    GET_SEARCH_BRANDS: state => state.search_brands
}

export const mutations = {
    SET_BRANDS: (state, payload) => {
        state.brands = {}
        for(let i=0;i<payload.length;++i) {
            let key = payload[i].name.charAt(0).toLowerCase()
            if (key >= '0' && key <= '9') {
                key = '0-9'
            } else if (key >= 'А' && key <= 'Я') {
                key = 'А-Я'
            }

            if (!state.brands[key]) {
                state.brands[key] = []
            }
            state.brands[key].push(payload[i])
        }
    },
    SET_SEARCH_BRANDS: (state, payload) => {
        state.search_brands = payload
    }
}

export const actions = {
    async fetchBrands({ commit }, payload) {
        commit('SET_BRANDS', await this.$api.get('brand'))
    },
    async searchBrands({ commit }, payload) {
        commit('SET_SEARCH_BRANDS', await this.$api.get('brand', { q: payload }))
    }
}
