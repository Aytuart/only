import Cookies from 'js-cookie'
import { $urls } from '~/common/urls.js'

export const state = () => ({
    token: null,
})

export const getters = {
    GET_TOKEN: (state) => state.token,
}

export const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token
        if (token) {
            Cookies.set('AuthToken', token, {expires: 30})
        } else {
            Cookies.remove('AuthToken', {expires: 30})
        }
    },
}

export const actions = {

    async signinFromCookies({ commit, dispatch }, token) {
        if (!token) {
            return
        }

        commit('SET_TOKEN', token)
        await dispatch('user/fetchUser', null, { root: true })
    },

    async createVerificationCode({ commit }, payload) {
        let resp = await this.$api.post('auth.code.create', payload)
        console.log(resp)
    },

    async signin({ commit, dispatch }, payload) {
        let resp = await this.$api.post('auth.signin', payload, 'signin')
        if (resp) {
            commit('SET_TOKEN', resp.token)
            await dispatch('user/fetchUser', null, { root: true })
        }
        // if (process.client) {
        //     window.location.reload(true)
        // }
    },

    async logout({ commit }, payload) {
        // this.$router.push('/')
        commit('SET_TOKEN', null)
        if (process.client) {
            window.location.reload(true)
        }
    },

}
