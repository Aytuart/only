export const state = () => ({
    account: 'only.kazakhstan',
    followers: null,
    posts: null,
})

export const getters = {
    GET_ACCOUNT: (state) => state.account,
    GET_FOLLOWERS: (state) => state.followers,
    GET_POSTS: (state) => state.posts
}

export const mutations = {
    SET_ACCOUNT: (state, payload) => {
        state.account = payload
    },
    SET_FOLLOWERS: (state, payload) => {
        state.followers = payload
    },
    SET_POSTS: (state, payload) => {
        state.posts = payload
    }
}

export const actions = {
    async fetch({ commit }) {
        try {
            const resp = await this.$api.get('instagram')
            commit('SET_ACCOUNT', resp.username)
            commit('SET_FOLLOWERS', resp.followers)
            commit('SET_POSTS', resp.posts)
        } catch (e) {
            //
        }
    }
}
