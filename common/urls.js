export const $urls = {
    baseUrl: process.env.NODE_ENV == 'development1' ? "http://admin.only.local.com" : "https://admin.only.kz",
    prefix: "api",

    auth: {
        prefix: "auth",
        code: {
            prefix: 'code',
            create: '',
            check: 'check'
        },
        signin: "signin",
    },

    user: {
        profile: '',
        prefix: "profile",
        product: "product",
        address: 'address',
        order: 'order',
        bonus: 'bonus',
    },
    zoodpay: {
        prefix: 'zoodpay',
        configuration: 'configuration'
    },

    order: {
        prefix: 'order',
        roll: 'roll'
    },

    instagram: 'instagram',
    page: "page",

    banner: "banner",
    shop: "shop",
    city: "city",
    payment: "payment",
    delivery: "delivery",
    deliveryCost: "delivery/cost",
    brand: "brand",
    post: "post",
    category: "category",
    filter: "filter", // {category}/{subcategory?}
    product: "product",
    productRecomend: "product/recomend",
    catalog: "catalog",
    subscribe: 'subscribe',
    checkout: 'checkout',
    settings: 'settings',

    url(name = null, args = []) {
        args = args.filter(arg => arg)
        return `${this.baseUrl}/${this.getUrl(name)}/${args.join("/")}`
    },

    getUrl(name = null) {
        if (!name) {
            return this.prefix;
        }

        let url = [], route = this, items = name.split(".")
        for (let i = 0; i < items.length; ++i) {
            if (route.prefix) {
                url.push(route.prefix)
            }
            route = route[items[i]]
        }

        url.push(typeof route == 'object' ? route.prefix : route)
        return url.filter(c => c).join("/")
    }
}
