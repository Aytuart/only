export default function({ app }, inject) {
    inject('get_seo_tags', (item) => {

        let seo = {
            meta: [
                {
                    hid: 'og_type',
                    name: 'og:type',
                    content: 'website'
                },
                {
                    hid: 'og_sitename',
                    name: 'og:sitename',
                    content: 'Only'
                },
                {
                    hid: 'og_url',
                    name: 'og:url',
                    content: 'https://only.kz'
                },
                {
                    hid: 'og_locale',
                    name: 'og:locale',
                    content: 'ru_KZ'
                },
                {
                    hid: 'twitter_card',
                    name: 'twitter:card',
                    content: 'summary'
                },
                {
                    hid: 'twitter_site',
                    name: 'twitter:site',
                    content: '@Only'
                },
                {
                    hid: 'twitter_creator',
                    name: 'twitter:creator',
                    content: '@Only'
                },
            ],
            link: [
                {
                    hid: 'canonical',
                    rel: 'canonical',
                    href: `https://only.kz${app.router.currentRoute.fullPath}`
                }
            ]
        }


        if (item) {
            if (item.name) {
                seo.title = item.name
            }
            if (item.seo_title) {
                seo.title = item.seo_title
                seo.meta.push({
                    hid: 'twitter_title',
                    name: 'twitter:title',
                    content: item.seo_title
                })
                seo.meta.push({
                    hid: 'og_title',
                    name: 'og:title',
                    content: item.seo_title
                })
            }

            if (item.seo_description) {
                seo.meta.push({
                    hid: 'description',
                    name: 'description',
                    content: item.seo_description
                })
                seo.meta.push({
                    hid: 'twitter_description',
                    name: 'twitter:description',
                    content: item.seo_description
                })
                seo.meta.push({
                    hid: 'og_description',
                    name: 'og:description',
                    content: item.seo_description
                })
            }

            if (item.seo_keywords) {
                seo.meta.push({
                    hid: 'keywords',
                    name: 'keywords',
                    content: item.seo_keywords
                })
            }

            let seo_image = item.seo_image ? item.seo_image : 'https://only.kz/images/logo.jpg'

            seo.meta.push({
                hid: 'og_image',
                name: 'og:image',
                content: seo_image
            })
            seo.meta.push({
                hid: 'twitter_image',
                name: 'twitter:image',
                content: seo_image
            })
        }

        return seo
    })

}
