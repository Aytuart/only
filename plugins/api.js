import { $urls } from '~/common/urls.js'

export default function({ $axios, store }, inject) {
    inject('api', {
        cache: {},

        async get(url, data = {}, pagination = null, error = 'default') {

            let key = this.getCacheKey(url, data)
            if (this.getFromCache(key)) {
                return this.getFromCache(key)
            }

            data['_method'] = 'GET'

            if (pagination) {
                store.commit('pagination/SET_PAGINATION', { key: pagination, value: { url: url, data: data } })
            }

            const resp = await this.sendRequest(url, data, error)

            if (process.client) {
                this.setCache(key, resp)
            }

            return resp
        },

        async paginate(key, page, data = {}) {
            let pagination = store.getters['pagination/GET_PAGINATION'](key)
            return await this.get(pagination.url, Object.assign(data, pagination.data, { page: page }))
        },

        async post(url, data = {}, error = 'default') {
            data['_method'] = 'POST'
            return await this.sendRequest(url, data, error)
        },

        async put(url, data = {}, error = 'default') {
            data['_method'] = 'PUT'
            return await this.sendRequest(url, data, error)
        },

        async delete(url, data = {}, error = 'default') {
            data['_method'] = 'DELETE'
            return await this.sendRequest(url, data, error)
        },

        async sendRequest(url, data, error = 'default') {
            let resp = null
            this.clearErrors()

            // if (process.client) {
            //     window.$nuxt.$root.$loading.start();
            // }

            try {
                resp = await $axios.$post(url.includes('://') ? url : $urls.url(url), data)
            } catch (e) {
                this.setError(error, e.response)
            }

            // if (process.client) {
            //     window.$nuxt.$root.$loading.finish()
            // }
            return resp
        },

        getCacheKey(url, data) {
            let key = url
            for(let k in data) {
                key+=(JSON.stringify(data[k] ? data[k] : '').split('\"').join(''))
            }
            return key
        },

        getFromCache(key) {
            return this.cache[key]
        },

        setCache(key, data) {
            this.cache[key] = data
        },

        setError(key, e) {
            let error = e

            if (e) {
                if (e.data) {
                    if (e.data.errors) {
                        error = []
                        Object.keys(e.data.errors).forEach((key) => {
                            error.push(e.data.errors[key].join(' '))
                        })
                        error = error.join(' ')
                    } else {
                        error = e.data.message
                    }
                } else if (e.message) {
                    error = e.message
                } else {

                }
            }

            store.commit('SET_ERROR', { [key]: error })
        },

        getError(key = 'default') {
            return store.getters['GET_ERROR'](key)
        },

        clearErrors() {
            store.commit('SET_ERRORS', {})
        }

    })
}
