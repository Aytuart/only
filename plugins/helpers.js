import moment from 'moment'

export default function({ store }, inject) {
    inject('isMobile', () => store.getters['GET_IS_MOBILE'])
    // inject('isMobile', () => true)

    inject('isValidPhone', (phone) => {
        return phone && /^[\d\(\)\s\+\-]+$/.test(phone)
    })

    inject('mask', (string, pattern, ch = '#') => {
        let masked = ''
        for(let i = 0, j = 0; i < pattern.length; ++i) {
            let char = ''
            if (pattern[i] == ch) {
                char = j < string.length ? string[j++] : ''
            } else if (j<string.length) {
                char = pattern[i]
            }
            masked += char
        }
        return masked
    })

    inject('get_hours', (seconds) => {
        return Math.floor(seconds / 3600)
    })

    inject('get_hours', (seconds) => {
        return Math.floor(seconds / 3600)
    })

    inject('get_minutes', (seconds) => {
        return Math.floor((seconds % 3600) / 60)
    })

    inject('get_seconds', (seconds) => {
        return (seconds % 3600) % 60
    })

    moment.locale('ru')
    inject('format_date', (value, format='D MMMM')  => {
        return moment(value).format(format)
    })

    inject('format_number', (number) => {
        return number ? number.toLocaleString().split(',').join(' ') : number
    })

    inject('str_limit', (str, cnt = 50) => {
        return str ? str.substring(0, cnt) : str
    })

    inject('pad', (str, size = 2, ch = '0') => {
        let s = str + ""
        while (s.length < size) s = ch + s
        return s
    })

    inject('settings', (key) => {
        return store.getters['settings/GET_BY_KEY'](key)
    })
}
