export default function({ store }, inject) {
    inject('toastr', {

        async notify(type, message) {
            const index = await store.dispatch('message/show', { type: type, message: message })

            setTimeout(() => {
                this.close(index)
            }, 3000)

        },

        success(message) {
            this.notify('success', message)
        },

        error(message) {
            this.notify('error', message)
        },

        close(index) {
            store.dispatch('message/close', index)
        }
    })
}
