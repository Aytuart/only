export default async function({ app, redirect }) {
    if (!app.$isSignedIn()) {
        return redirect('/')
    }
}
