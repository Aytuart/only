export default async function({ store, route, error, app }) {
    await store.dispatch('content/fetchPost', route)
    if (app.$api.getError('post')) {
        error({ statusCode: 404, message: "" })
    }
}
