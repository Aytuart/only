export default async function({ store, route }) {
    let queue = []

    if (!store.getters['content/GET_LAST_POSTS']) {
        queue.push(store.dispatch('content/fetchLastPosts'))
    }

    if (!store.getters['content/GET_BANNERS']) {
        queue.push(store.dispatch('content/fetchBanners'))
    }

    if (!store.getters['product/GET_NEWS']) {
        queue.push(store.dispatch('product/fetchNews', route))
    }
    if (!store.getters['product/GET_HITS']) {
        queue.push(store.dispatch('product/fetchHits', route))
    }
    if (!store.getters['product/GET_SALES']) {
        queue.push(store.dispatch('product/fetchSales', route))
    }

    return Promise.all(queue)

}
