export default async function({ store, route }) {
    return store.dispatch('user/fetchOrders')
}
