export default async function({ store, route }) {
    if (!store.getters['brand/GET_BRANDS']) {
        return store.dispatch('brand/fetchBrands')
    }
}
