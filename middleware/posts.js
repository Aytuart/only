export default async function({ store, route }) {
    if (!store.getters['content/GET_POSTS']) {
        return store.dispatch('content/fetchPosts', route)
    }
}
