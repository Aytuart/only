export default async function({ store }) {
    let queue = []

    if (!store.getters['content/GET_SHOPS']) {
        queue.push(store.dispatch('content/fetchShops'))
    }

    if (!store.getters['content/GET_CITIES']) {
        queue.push(store.dispatch('content/fetchCities'))
    }

    if (!store.getters['content/GET_PAYMENT_TYPES']) {
        queue.push(store.dispatch('content/fetchPaymentTypes'))
    }

    if (!store.getters['content/GET_DELIVERY_TYPES']) {
        queue.push(store.dispatch('content/fetchDeliveryTypes'))
    }

    return Promise.all(queue)
}
