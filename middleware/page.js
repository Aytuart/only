export default async function({ store, route }) {
    return store.dispatch('page/fetchPage', route.path.split('/')[1])
}
