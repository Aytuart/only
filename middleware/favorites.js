export default async function({ store, route }) {
    return store.dispatch('product/fetchFavorites', store.getters['favorites/GET_FAVORITES'])
}
