export default async function({ store }) {
    if (!store.getters['user/GET_ADDRESSES']) {
        return store.dispatch('user/fetchAddresses')
    }
}
