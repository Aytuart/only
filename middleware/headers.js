export default function(req, res, next) {
    res.setHeader('Vary', 'User-Agent')
    next()
}
