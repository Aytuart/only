export default async function({ store, route, app, error }) {
    await store.dispatch('product/fetchBrand', route)
    if (app.$api.getError('brand')) {
        error({ statusCode: 404, message: "" })
    }

    if (!await store.dispatch('category/setCategory', route)) {
        error({ statusCode: 404, message: "qwe" })
    }

    if (!await store.dispatch('category/setSubCategory', route)) {
        error({ statusCode: 404, message: "" })
    }

    let queue = []

    if (!store.getters['content/GET_LAST_POSTS']) {
        queue.push(store.dispatch('content/fetchLastPosts'))
    }

    if (route.params.product) {// No need to fetch products and filters
        queue.push(store.dispatch('product/fetchProduct', route))
        queue.push(store.dispatch('product/fetchRecomended', route))
        queue.push(store.dispatch('product/fetchReviews', route))
        queue.push(store.dispatch('product/fetchQuestions', route))
    } else {
        queue.push(store.dispatch('filter/fetchFilters', route))
        queue.push(store.dispatch('product/fetchProducts', route))
    }

    await Promise.all(queue)
    if (app.$api.getError('product')) {
        error({ statusCode: 404, message: "" })
    }

}
