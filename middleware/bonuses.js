export default async function({ store }) {
    if (!store.getters['user/GET_BONUSES']) {
        return store.dispatch('user/fetchBonuses')
    }
}
