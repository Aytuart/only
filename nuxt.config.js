export default {
    mode: "universal",
    head: {
        htmlAttrs: {
            lang: "en"
        },
        title: "Only",
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content:
                    "width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"
            },
            {
                hid: "description",
                name: "description",
                content: process.env.npm_package_description || ""
            }
        ],
        link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
        script: [
            // { src: 'https://code.jivosite.com/widget.js', 'data-jv-id': 'PYhJZ9ohfZ', async: true }
        ]
    },

    loading: "components/partials/preloader.vue",

    css: ["@/assets/app.styl"],

    serverMiddleware: ["@/middleware/headers.js"],
    plugins: [
        { src: "@/plugins/auth.js" },
        { src: "@/plugins/helpers.js" },
        { src: "@/plugins/seo.js" },
        { src: "@/plugins/flags.js" },
        { src: "@/plugins/api.js" },
        { src: "@/plugins/toastr.js" },
        { src: "@/plugins/yandex.js", mode: "client" },
        { src: "@/plugins/vue-the-mask.js", ssr: false },
        { src: "@/plugins/vue-swiper.js", ssr: false },
        { src: "@/plugins/vue-slider-component.js", ssr: false },
        { src: "@/plugins/vue-ripple-directive.js", ssr: false },
        { src: "@/plugins/vue-star-component.js", ssr: false },
        { src: "@/plugins/vue-bottom-sheet.js", ssr: false }
    ],

    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        "@nuxtjs/axios",
        "nuxt-svg-loader",
        "@nuxtjs/google-analytics",
        [
            "nuxt-lazy-load",
            {
                // These are the default values
                images: true,
                videos: false,
                audios: false,
                iframes: false,
                native: false,
                polyfill: true,
                directiveOnly: false,

                // Default image must be in the static folder
                defaultImage: "/images/placeholder.svg",

                // To remove class set value to false
                loadingClass: false,
                loadedClass: false,
                appendClass: false,

                observerConfig: {
                    // See IntersectionObserver documentation
                }
            }
        ],
        [
            "@nuxtjs/yandex-metrika",
            {
                id: "56849152",
                webvisor: true,
                clickmap: true,
                // useCDN:false,
                trackLinks: true,
                accurateTrackBounce: true
            }
        ]
    ],
    googleAnalytics: {
        id: "G-JL6QPNV77Q"
    },
    build: {
        extend(config, ctx) {}
    }
};
